// testbench.v

module testbench();
  reg [3:0]a, b;
  wire [3:0]y1, y2, y3, y4, y5, y6;
  
  //Instantiate device under test
  gates dut (a, b, y1, y2, y3, y4, y5, y6);
  // Apply inputs one at a time
  initial begin
    a = 0000; b = 0000; #10;
    a = 1111; b = 0000; #10;
    a = 0000; b = 1111; #10;
    a = 1111; b = 1111; #10;
    a = 0001; #10;
  end
endmodule
// signext.v
// Sign Extender
// Tendayi F. Kamucheka
// MCMiP
module signext	#( parameter WIDTH = 16)
				 ( input  [WIDTH-1:0] a,
				   output [31:0] y);
	
	assign y = { {16{a[WIDTH-1]}}, a};

endmodule
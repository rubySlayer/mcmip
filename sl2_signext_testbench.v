module testbench();
	reg [15:0] signext;
	reg [31:0] leftshft;

	wire [31:0] extnsn;
	wire [31:0] shftd;

	signext dut(signext,extnsn); 

	sl2 dut2(leftshft, shftd);

	initial
		begin
			signext  <= 16'b1000000000000000;
			leftshft <= 32'b1;
		end

endmodule
// mux6 testbench
// mux6Testbench.v
// Tendayi Kamucheka

module mux6testbench ();
  reg  [31:0] a, b, c, d, e, f;
  reg  [2:0] muxCtrl;
  wire [31:0] mux6Out, mux2Out, mux4Out;
  
  mux2 dut  (a, b, muxCtrl[1], mux2Out);
  mux4 dut1 (c, d, e, f, muxCtrl[2:1], mux4Out);
  mux6 dut2 (a, b, c, d, e, f, muxCtrl, mux6Out);
  
  initial
  begin
    a = 32'b1; b = 32'b11; c = 32'b111; d = 32'b1111; e = 32'b11111; f = 32'b111111;
    muxCtrl = 000; #20;
    muxCtrl = 001; #20;
    muxCtrl = 010; #20;
    muxCtrl = 011; #20;
    muxCtrl = 100; #20;
    muxCtrl = 101; #20;
    muxCtrl = 110; #20;
    muxCtrl = 111; #20;
  end
endmodule

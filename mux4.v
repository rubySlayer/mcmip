//  mux4.v
//  4:1 Multiplexer
//  Tendayi F. Kamucheka

module mux4 #( parameter WIDTH = 32)
             ( input  [WIDTH-1:0] a, b, c, d,
               input  [1:0] muxCtrl,
               output [WIDTH-1:0] muxOut );
                
  wire [WIDTH-1:0] hiOut;
  wire [WIDTH-1:0] loOut;
               
  // Using 2:1 multiplexer
  mux2 hiMux     (a, b, muxCtrl[0], hiOut);
  mux2 loMux     (c, d, muxCtrl[0], loOut);
  mux2 finalMux  (hiOut, loOut, muxCtrl[1], muxOut);
  
endmodule


    
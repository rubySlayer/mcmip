// sl2.v
// Left Shift (Multiply By 4)
// Tendayi F. Kamucheka
// MCMiP
module sl2	#( parameter WIDTH = 32)
			 ( input  [WIDTH-1:0] a,
		       output [31:0] y);

	// shift left by 2
	assign y = { a[WIDTH-3:0], 2'b00 };

endmodule
// 6:1 Multiplexer
// mux6.v
// Tauqeer Ahmad
module mux6  #( parameter WIDTH = 32 )
              ( input [WIDTH-1:0] a, 
                input [WIDTH-1:0] b, 
                input [WIDTH-1:0] c, d, e, f,
                input [2:0] muxCtrl,
                output [ WIDTH-1:0] muxOut);
    
   wire [WIDTH-1:0] hiOut;
   wire [WIDTH-1:0] loOut;
   
   mux2 hiMux     (a,b, muxCtrl[0], hiOut);
   mux4 lowMux    (c,d,e,f, muxCtrl[1:0], loOut);
   mux2 finalMux  (hiOut, loOut, muxCtrl[2], muxOut);
   
 endmodule 
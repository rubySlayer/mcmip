// alu_testbench.v
// ALU Testbench

module alu_testbench();

  reg   [31:0] srca, srcb; 

  // Input Signals
  reg   [2:0]  alucontrol;

  // ALU Control Signal
  wire  [31:0] aluout;    
 
  // ALU Output Signal
  wire         zero;       
  
  // Zero Flag
  // Instantiate Device Under Test
    alu dut( srca, srcb, alucontrol, aluout, zero );
      initial begin
          srca <= 32'b1; srcb<= 32'b1; alucontrol = 3'b000; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = 32'b0; srcb = 32'b1; alucontrol = 3'b001; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = { {24'b0},{8'b1} }; srcb = { {24'b0},{8'b1} }; alucontrol = 3'b010; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = 32'b1; srcb = 32'b1; alucontrol = 3'b100; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = 32'b0; srcb = 32'b1; alucontrol = 3'b101; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = { {16'b0},{16'b1} }; srcb = { {24'b0}, {8'b1} }; alucontrol = 3'b110; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = 32'b0; srcb = 32'b1; alucontrol = 3'b111; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
          srca = 32'b1; srcb = 32'b0; alucontrol = 3'b011; #10;
              $display("@%0dns zero = %b, aluout = %b", $time, zero, aluout );
      end
endmodule

// alu.v
// Arithmetic Logic Unit
module alu( input   [31:0]  srca,
            input   [31:0]  srcb,
            input   [2:0]   alucontrol,
            output  reg [31:0]  aluout,
            output  reg     zero );
  
  always@( * )
  
    begin
      case( alucontrol )
        3'b000: { aluout } <= srca & srcb;
        3'b001: { aluout } <= srca | srcb;
        3'b010: { aluout } <= srca + srcb;
        3'b100: { aluout } <= srca & ~srcb;
        3'b101: { aluout } <= srca | ~srcb;
        3'b110: { aluout } <= srca + ~srcb + 1'b1;
        3'b111: begin
                  if ( srca < srcb )
                    { aluout } <= 32'b1;
                end
        default: { aluout } <= 32'b0 ;
      endcase
      // Set Zero Flag
      if ( aluout == 32'b0 )
        zero = 1'b1;
      else zero = 1'b0;
    end
    
endmodule